#!/usr/bin/env python

import roslib; roslib.load_manifest( 'rviz_plugin_tutorials' )
from geometry_msgs.msg import Twist
import rospy
from math import cos, sin
import tf

topic = 'test_twist'
publisher = rospy.Publisher( topic, Twist )

rospy.init_node( 'test_twist' )

br = tf.TransformBroadcaster()
rate = rospy.Rate(10)
radius = 5
angle = 0

dist = 3
while not rospy.is_shutdown():

    t = Twist()
    #t.header.frame_id = "/base_link"
    #t.header.stamp = rospy.Time.now()
   
    t.linear.x = sin( 10 * angle )
    t.linear.y = sin( 20 * angle )
    t.linear.z = sin( 40 * angle )

    t.angular.x = sin( 10 * angle )
    t.angular.y = sin( 20 * angle )
    t.angular.z = sin( 40 * angle )

    publisher.publish( t )

    #br.sendTransform((radius * cos(angle), radius * sin(angle), 0),
    #                 tf.transformations.quaternion_from_euler(0, 0, angle),
    #                 rospy.Time.now(),
    #                 "base_link",
    #                 "map")
    angle += .01
    rate.sleep()

