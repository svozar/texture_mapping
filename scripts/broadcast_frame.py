#!/usr/bin/env python

import roslib
import sys
import rospy
import cv2
import logging
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

logging.basicConfig()

if len(sys.argv)<2:
  rospy.logerr("No image file specified")
  sys.exit(0)
if len(sys.argv)<3:
  topic_name = "image_topic"
else:
  topic_name = sys.argv[2]

rospy.init_node('frame_broadcaster', anonymous=True)
img = cv2.imread(sys.argv[1])

image_pub = rospy.Publisher(topic_name, Image)
bridge = CvBridge()

r = rospy.Rate(10)

while not rospy.is_shutdown():
  image_pub.publish(bridge.cv2_to_imgmsg(img, "bgr8"))
  r.sleep()

