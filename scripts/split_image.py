#!/usr/bin/env python

import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class image_splitter:

  def __init__(self):
    self.image_pub_left = rospy.Publisher("image_topic_left",Image)
    self.image_pub_right = rospy.Publisher("image_topic_right",Image)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("camera/image_raw",Image,self.callback)

  def callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError, e:
      print e
        
    h, w, d = cv_image.shape
    image_left = cv_image[0:h, 0:(w/2)]
    image_right = cv_image[0:h, (w/2):w]

    try:
      self.image_pub_left.publish(self.bridge.cv2_to_imgmsg(image_left, "bgr8"))
      self.image_pub_right.publish(self.bridge.cv2_to_imgmsg(image_right, "bgr8"))
    except CvBridgeError, e:
      print e

def main(args):
  isp = image_splitter()
  rospy.init_node('image_splitter', anonymous=True)
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print "Shutting down"

if __name__ == '__main__':
    main(sys.argv)
