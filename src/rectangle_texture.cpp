/*
 * Copyright (c) 2012, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <boost/bind.hpp>

#include <OgreManualObject.h>
#include <OgreMaterialManager.h>
#include <OgreRectangle2D.h>
#include <OgreEntity.h>
#include <OgreRenderSystem.h>
#include <OgreRenderWindow.h>
#include <OgreRoot.h>
#include <OgreSceneManager.h>
#include <OgreSceneNode.h>
#include <OgreTextureManager.h>
#include <OgreViewport.h>
#include <OgreTechnique.h>
#include <OgreCamera.h>

#include <tf/transform_listener.h>

#include "rviz/display_context.h"
#include "rviz/frame_manager.h"
#include "rviz/validate_floats.h"

#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/Pose.h>

#include "rectangle_texture.h"

#include <ros/console.h>

namespace rviz
{

RectangleTexture::RectangleTexture()
  : ImageDisplayBase()
  , texture_()
{
  got_float_image_ = false;
  aspect_ratio_ = 1.0;
  scale_property_  = new rviz::FloatProperty("Scale", 1.0, "Image Scale", this, SLOT( updateScale() ) );
  tf_frame_property_ = new rviz::RosTopicProperty("Transform Frame", "/camera/blah", "geometry_msgs/Pose", "Transform Frame", this, SLOT( updateTFFrame() ) );
}

void RectangleTexture::updateTFFrame(){

  ROS_INFO("Transform topic changed to %s", tf_frame_property_->getTopicStd().c_str());

  position_sub_ = n_.subscribe(tf_frame_property_->getTopicStd(), 10, &RectangleTexture::positionCallback, this);
}

void RectangleTexture::updateScale(){

  plane_node_->setScale(Ogre::Vector3(aspect_ratio_,1,1)*scale_property_->getFloat()*.01);
  ROS_INFO("Scale Changed to %f", scale_property_->getFloat());

}

void RectangleTexture::onInitialize()
{
  ImageDisplayBase::onInitialize();
  {
    static uint32_t count = 0;
    std::stringstream ss;
    ss << "RectangleTexture" << count++;
    //img_scene_manager_ = Ogre::Root::getSingleton().createSceneManager(Ogre::ST_GENERIC, ss.str());
    img_scene_manager_ = context_->getSceneManager();
  }

  img_scene_node_ = img_scene_manager_->getRootSceneNode()->createChildSceneNode();

  {
    static int count = 0;
    std::stringstream ss;
    ss << "RectangleTextureObject" << count++;

    Ogre::SceneNode* frame_node = img_scene_node_->createChildSceneNode();
    Ogre::Entity* plane = img_scene_manager_->createEntity(Ogre::SceneManager::PT_PLANE);
    plane_node_ = frame_node->createChildSceneNode(Ogre::Vector3(0,0,0));

    plane_node_->attachObject(plane);

    plane_node_->pitch(Ogre::Degree(90));
    plane_node_->scale(Ogre::Vector3(0.01, 0.01, 0.01));

    ss << "Material";
    material_ = Ogre::MaterialManager::getSingleton().create( ss.str(), Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
    material_->setSceneBlending( Ogre::SBT_REPLACE );
    material_->setDepthWriteEnabled(true);
    material_->setReceiveShadows(false);
    material_->setDepthCheckEnabled(true);

    material_->getTechnique(0)->setLightingEnabled(false);
    Ogre::TextureUnitState* tu = material_->getTechnique(0)->getPass(0)->createTextureUnitState();
    tu->setTextureName(texture_.getTexture()->getName());
    tu->setTextureFiltering( Ogre::TFO_NONE );

    material_->setCullingMode(Ogre::CULL_NONE);

    plane->setMaterial(material_);

  }


  updateTFFrame();

}

RectangleTexture::~RectangleTexture()
{
  if ( initialized() )
  {
    img_scene_node_->getParentSceneNode()->removeAndDestroyChild( img_scene_node_->getName() );
  }
}

void RectangleTexture::onEnable()
{
  ImageDisplayBase::subscribe();
  plane_node_->setVisible(true);
}

void RectangleTexture::onDisable()
{
  ImageDisplayBase::unsubscribe();
  clear();
  plane_node_->setVisible(false);
}


void RectangleTexture::clear()
{
  texture_.clear();
}

void RectangleTexture::update( float wall_dt, float ros_dt )
{
  try
  {
    texture_.update();

    //make sure the aspect ratio of the image is preserved
    float img_height = texture_.getHeight();
    float img_width = texture_.getWidth();
    float aspect_ratio = img_width / img_height;

    if (img_height == 0 || img_width == 0){ //There's no image and aspect_ratio will be NaN
      return;
    }
    else if (aspect_ratio_ != aspect_ratio){
      plane_node_->scale(Ogre::Vector3(aspect_ratio,1, 1));
      aspect_ratio_ = aspect_ratio;
    }

  }
  catch( UnsupportedImageEncoding& e )
  {
    setStatus(StatusProperty::Error, "Image", e.what());
  }
}

void RectangleTexture::reset()
{
  ImageDisplayBase::reset();
  clear();
}

void RectangleTexture::positionCallback(const geometry_msgs::Pose::ConstPtr& msg)
{

  plane_node_->setPosition(Ogre::Vector3(msg->position.x,
                                         msg->position.y,
                                         msg->position.z));

  plane_node_->setOrientation(Ogre::Quaternion( msg->orientation.x,
                                                msg->orientation.y,
                                                msg->orientation.z,
                                                msg->orientation.w));

  return;

}
/* This is called by incomingMessage(). */
void RectangleTexture::processMessage(const sensor_msgs::Image::ConstPtr& msg)
{
  bool got_float_image = msg->encoding == sensor_msgs::image_encodings::TYPE_32FC1 ||
      msg->encoding == sensor_msgs::image_encodings::TYPE_16UC1 ||
      msg->encoding == sensor_msgs::image_encodings::TYPE_16SC1 ||
      msg->encoding == sensor_msgs::image_encodings::MONO16;

  if ( got_float_image != got_float_image_ )
  {
    got_float_image_ = got_float_image;
  }
  texture_.addMessage(msg);
}

} // namespace rviz

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS( rviz::RectangleTexture, rviz::Display )
